import sys
import os


def main():
    filepath = sys.argv[1]

    if not os.path.isfile(filepath):
        print("File path {} does not exist. Exiting...".format(filepath))
        sys.exit()

    first_player = sys.argv[2]
    second_player = sys.argv[3]

    p1_score = 0
    p2_score = 0
    game_count = 0

    f = open(filepath)
    line = f.readline()
    while line:
        split = line.split(": ")
        nameA = split[0]
        scoreA = float(split[1])

        line = f.readline()
        split = line.split(": ")
        nameB = split[0]
        scoreB = float(split[1])

        if nameA == first_player:
            if scoreA > scoreB:
                p1_score+=1
            elif scoreB > scoreA:
                p2_score+=1
        elif nameB == first_player:
            if scoreA > scoreB:
                p2_score+=1
            elif scoreB > scoreA:
                p1_score+=1

        game_count+=1
        line = f.readline()
    f.close()

    print("RESULTS:")
    print(str(first_player) + " " + str(p1_score) + "/" + str(game_count) + " " + str(p1_score*100/game_count) + "%")
    print(str(second_player) + " " + str(p2_score) + "/" + str(game_count) + " " + str(p2_score*100/game_count) + "%")


if __name__ == '__main__':
    main()