package za.ac.sun.cs.ingenious.games.othello.exceptions;

/**
 * Raised when a move is made which is not valid (impossible) within 
 * the current game or specific game setting.
 */
public class InvalidMoveException extends Exception {

    public InvalidMoveException(String msg)
	{
        super(msg);
    }
}
