package za.ac.sun.cs.ingenious.core.util.misc;

import java.io.Serializable;

/**
 * Class representing a coordinate consisting of two integer positions, x and y.
 */
public class Coord implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private final int xPosition;
	private final int yPosition;
	
	public Coord(int x, int y){
		this.xPosition = x;
		this.yPosition = y;
	}
	
	public Coord(double d, double e) {
		xPosition = (int) Math.round(d);
		yPosition = (int) Math.round(e);
	}
	
	public static Coord hexRound(double q, double r){
		double x =q;
		double z =r;
		double y = -x-z;
		
		int rx = (int) Math.round(x);
		int rz = (int)  Math.round(z);
		int ry = (int) Math.round(y);
		
	    double x_diff = Math.abs(rx - x);
	    double y_diff = Math.abs(ry - y);
	    double z_diff = Math.abs(rz - z);
	    
	    if (x_diff > y_diff && x_diff > z_diff){
	        rx = -ry-rz ;
	    }else if( y_diff > z_diff) {
			ry = -rx-rz;
		} else{
	        rz = -rx-ry;
	    }
	    
		return new Coord(rx,rz);
	}

	public int getX(){
		return xPosition;
	}
	
	public int getY(){
		return yPosition;
	}
	
	public Coord add(Coord coord){
		return new Coord(coord.xPosition+this.xPosition,coord.yPosition+this.yPosition);
	}
	
	public Coord sub(Coord coord){
		return new Coord(this.xPosition-coord.xPosition,this.yPosition-coord.yPosition);
	}
	
	public Coord negate(){
		return new Coord(-xPosition,-yPosition);
	}
	
	@Override
	public boolean equals(Object o){
		
		if (!(o instanceof Coord)){
			return false;
		}
		
		Coord oCoord = (Coord) o;
		
		if(oCoord.getX() == this.getX()){
			if(oCoord.getY()==this.getY()){
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public int hashCode(){
		int hash = 7;
	    hash = 71 * hash + this.xPosition;
	    hash = 71 * hash + this.yPosition;
	    return hash;
	}
	
	@Override
	public String toString(){
		return "("+this.xPosition+" , "+ this.yPosition+")";
	}
}
