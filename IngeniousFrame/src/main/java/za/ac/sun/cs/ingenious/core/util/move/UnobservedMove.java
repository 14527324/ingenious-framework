package za.ac.sun.cs.ingenious.core.util.move;

import za.ac.sun.cs.ingenious.core.Move;

/**
 * Objects of this class are used to inform players that some move has been executed by
 * some player, without letting on any information on which move it was.
 * 
 * Some games may also contain partially observable moves, where some aspects of the move
 * are hidden while others are not. For those cases, game developers should <b>not</b>
 * extend this class and instead create their own move-classes by implementing the
 * {@link Move} interface
 * 
 * @author Michael Krause
 */
public class UnobservedMove implements Move {

	private static final long serialVersionUID = 1L;

	private final int playerID;

	/**
	 * @param playerID The ID of the player executing the move
	 */
	public UnobservedMove(int playerID) {
		this.playerID = playerID;
	}

	/**
	 * @return The ID of the player executing the move
	 */
	@Override
	public int getPlayerID() {
		return playerID;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof UnobservedMove)) {
			return false;
		} else {
			return (this.playerID == ((UnobservedMove) obj).playerID);
		}
	}

	@Override
	public int hashCode() {
		return playerID;
	}

	@Override
	public String toString() {
		return "UnobservedMove, playerID: " + playerID;
	}
}
