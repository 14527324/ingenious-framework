package za.ac.sun.cs.ingenious.core.util.search;

import za.ac.sun.cs.ingenious.core.GameState;
import com.rits.cloning.Cloner;

class SearchNodeSimple<S extends GameState> implements SearchNode<S> {
	
	private static final Cloner cloner = new Cloner();
	private final S state;
	private double value;
	
	private SearchNodeSimple(S state) { //private because it should be uninstanciable
		this.state = stateDeepCopy(state);
		this.value = 0;
	}
	
	public static <SS extends GameState> SearchNodeSimple<SS> newInstance(SS state) { //Since private constructor => factory method
		return new SearchNodeSimple<SS>(state);
	}
	
	// returns defensive copy
	public final S getState() {
		return stateDeepCopy(this.state);
	}
	
	public final double getValue() {
		return value;
	}
	
	public final void addValue(double add) {
		this.value += add;
	}
	
	// TODO: change?
	public String toString() {
		return ("SearchNode:\n\tState = " + getState().toString() + "\n\tvalue = " + getValue());
	}
	
	private final S stateDeepCopy(S state) {
		return cloner.deepClone(state); // TODO: copied idea from GameState... should check
	}
}
