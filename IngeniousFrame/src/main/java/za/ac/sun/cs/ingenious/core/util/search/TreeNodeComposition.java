package za.ac.sun.cs.ingenious.core.util.search;

import za.ac.sun.cs.ingenious.core.GameState;
import java.util.List;

public class TreeNodeComposition<S extends GameState, C, P> implements TreeNode<S, C, P> {
	
	SearchNode<S> searchNode;
	P parent;
	List<C> children;
	
	public TreeNodeComposition(S state, P parent, List<C> children) {
		searchNode = SearchNodeSimple.newInstance(state);
		this.parent = parent;
		this.children = children;
	}
	
	// SearchNode interface methods:
	public S getState() {
		return searchNode.getState();
	}
	
	public double getValue() {
		return searchNode.getValue();
	}
	
	public void addValue(double add) {
		searchNode.addValue(add);
	}
	
	// TODO: change?
	public String toString() {
		return ("TreeNodeComposition:\n\tState = " + getState().toString() + "\n\tvalue = " + getValue() +
		        "\n\tParent (state) = " + parent + "\n\tChildren = " + getChildren());
	}
	
	// TreeSearchNode interface methods:
	public P getParent() {
		return this.parent;
	}
	
	public void setParent(P parent) {
		this.parent = parent;
	}
	
	public List<C> getChildren() {
		return this.children;
	}
	
	public void setChildren(List<C> children) {
		this.children = children;
	}

	public void addChild(C child) {
		children.add(child);
	}
	
	public void addChildren(List<C> newChildren) {
		for (C child : newChildren) {
			children.add(child);
		}
	}
	
}

// TODO: comments
