package za.ac.sun.cs.ingenious.core.util.search.mcts;

import za.ac.sun.cs.ingenious.core.util.search.TreeNode;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Action;
import java.util.List;

public interface MctsNode<S extends GameState, C, P> extends TreeNode<S, C, P> {

	// SearchNode interface:
	// TODO: add after completion... And then ask if you should leave or remove
	
	// TreeNode interface:
	// TODO: add after completion... And then ask if you should leave or remove
	
	public int getVisitCount();
	
	public void incVisitCount();
	
	public void decVisitCount();
	
	public void incChildVisitCount(C child);
	
	public int getChildVisitCount(C child);
	
	public double getChildValue(C child);
	
	public List<Action> getUnexploredChildren(); 
	
	public  boolean isUnexploredEmpty();
	
	public C getAnUnexploredChild(GameLogic<S> logic);
	
	public Action getPrevAction();
	
	public C getSelfAsChildType();
	
}

// TODO: comments!
