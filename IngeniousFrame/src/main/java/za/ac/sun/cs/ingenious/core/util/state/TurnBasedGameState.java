package za.ac.sun.cs.ingenious.core.util.state;

import za.ac.sun.cs.ingenious.core.GameState;

/**
 * Represents the state of a game where players take turns, i.e: per state there is
 * exactly one person that can act.
 * This is meant to be used with a {@link TurnBasedGameLogic}
 * 
 * @author Michael Krause
 */
public abstract class TurnBasedGameState extends GameState {

	/**
	 * The ID of the player that will act next.
	 */
	public int nextMovePlayerID;
	
	public TurnBasedGameState(int firstPlayer, int numPlayers) {
		super(numPlayers);
		this.nextMovePlayerID = firstPlayer;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + nextMovePlayerID;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TurnBasedGameState other = (TurnBasedGameState) obj;
		if (nextMovePlayerID != other.nextMovePlayerID)
			return false;
		return true;
	}

}
