package za.ac.sun.cs.ingenious.games.bomberman.ui;

import za.ac.sun.cs.ingenious.games.bomberman.ui.drawableengine.Sprite;
import za.ac.sun.cs.ingenious.games.bomberman.ui.drawableengine.SpriteDrawable;
import za.ac.sun.cs.ingenious.games.bomberman.ui.drawableengine.SpriteStore;

public class TileDrawable extends SpriteDrawable {
	/* FIXME to load from JAR */

	public static final String resLocation = "bomberman/";
	public static final int    tileSize    = 30;

	public TileDrawable(char c, int x, int y) {
		super(TileDrawable.getSpriteFromChar(c), 0, x * 30, y * 30);
	}

	public static Sprite getSpriteFromChar(char c) {
		switch (c) {
			case '#':
				return SpriteStore.get().getSprite(resLocation + "wall.png");
			case '+':
				return SpriteStore.get().getSprite(resLocation+"cradle.png");
			case ' ': return SpriteStore.get().getSprite(resLocation+"space.png");

			case '*': return SpriteStore.get().getSprite(resLocation+"explode.png");

			case 'A': return SpriteStore.get().getSprite(resLocation+"A.png");
			case 'B': return SpriteStore.get().getSprite(resLocation+"B.png");
			case 'C': return SpriteStore.get().getSprite(resLocation+"C.png");
			case 'D': return SpriteStore.get().getSprite(resLocation+"D.png");

			case 'a': return SpriteStore.get().getSprite(resLocation+"_A.png");
			case 'b': return SpriteStore.get().getSprite(resLocation+"_B.png");
			case 'c': return SpriteStore.get().getSprite(resLocation+"_C.png");
			case 'd': return SpriteStore.get().getSprite(resLocation+"_D.png");

			case '1': return SpriteStore.get().getSprite(resLocation+"1.png");
			case '2': return SpriteStore.get().getSprite(resLocation+"2.png");
			case '3': return SpriteStore.get().getSprite(resLocation+"3.png");
			case '4': return SpriteStore.get().getSprite(resLocation+"4.png");
			case '5': return SpriteStore.get().getSprite(resLocation+"5.png");
			case '6': return SpriteStore.get().getSprite(resLocation+"6.png");
			case '7': return SpriteStore.get().getSprite(resLocation+"7.png");
			case '8': return SpriteStore.get().getSprite(resLocation+"8.png");
			case '9': return SpriteStore.get().getSprite(resLocation+"9.png");

			case '!': return SpriteStore.get().getSprite(resLocation+"excl.png");
			case '&': return SpriteStore.get().getSprite(resLocation+"and.png");
			case '$': return SpriteStore.get().getSprite(resLocation+"dollar.png");
			default: new ClassNotFoundException("Image: "+c+" not known.").printStackTrace(); return null;
		}


	}

}
