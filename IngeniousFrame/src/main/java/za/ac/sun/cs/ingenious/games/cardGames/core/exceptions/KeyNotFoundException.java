package za.ac.sun.cs.ingenious.games.cardGames.core.exceptions;

/**
 * The KeyNotFoundException.
 * 
 * Will be raised if the key (which is a Card) could not be found in the TreeMap of the CardGameState
 * 
 * @author Joshua Wiebe
 */
public class KeyNotFoundException extends Exception{

	/** The serialVersionUID. */
	private static final long serialVersionUID = 327482937428L;
	
	/**
	 * Instantiates a new KeyNotFoundException.
	 *
	 * @param Message for super class.
	 */
	public KeyNotFoundException(String message){
		super(message);
	}
	
}
