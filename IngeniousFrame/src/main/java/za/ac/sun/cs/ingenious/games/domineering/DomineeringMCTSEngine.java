package za.ac.sun.cs.ingenious.games.domineering;

import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.Constants;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.sensor.PerfectInformationActionSensor;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.search.mcts.legacy.MCTS;
import za.ac.sun.cs.ingenious.search.mcts.legacy.MCTSDescender;
import za.ac.sun.cs.ingenious.search.mcts.legacy.MCTSNode;
import za.ac.sun.cs.ingenious.search.mcts.legacy.RandomPolicy;
import za.ac.sun.cs.ingenious.search.mcts.legacy.SimpleUpdater;
import za.ac.sun.cs.ingenious.search.mcts.legacy.UCT;

public class DomineeringMCTSEngine extends DomineeringEngine {
	
	public DomineeringMCTSEngine(EngineToServerConnection toServer) {
		super(toServer);
	}

	@Override
	public void setZobrist(ZobristHashing zobristHashing) {

	}

	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		if (board == null) {
			throw new RuntimeException(engineName() + ": board was null when GenMoveMessage was received! (Perhaps no InitGameMessage was sent?)");
		}
		// This engine chooses a move with MCTS
		MCTSNode<TurnBasedSquareBoard> root = new MCTSNode<TurnBasedSquareBoard>(board, logic, null, null);
		return new PlayActionMessage(MCTS.generateAction(root,
				new RandomPolicy<TurnBasedSquareBoard>(logic, new DomineeringFinalEvaluator(), new PerfectInformationActionSensor<>(), false),
				new MCTSDescender<TurnBasedSquareBoard>(logic, new UCT<>(), new PerfectInformationActionSensor<>()), 
				new SimpleUpdater<MCTSNode<TurnBasedSquareBoard>>(),
				Constants.TURN_LENGTH));
	}
	
	@Override
	public String engineName() {
		return "DomineeringMCTSEngine";		
	}
	
}
