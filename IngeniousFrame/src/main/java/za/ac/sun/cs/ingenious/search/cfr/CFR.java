package za.ac.sun.cs.ingenious.search.cfr;

import com.esotericsoftware.minlog.Log;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;
import za.ac.sun.cs.ingenious.search.InformationSetDeterminizer;

/**
 * This implements counterfactual regret minimization for any game. This implementation follows the
 * terminology and pseudocode given in "An Introduction to Counterfactual Regret Minimization" by 
 * Todd W. Neller and Marc Lanctot.
 * 
 * Note that this algorithm needs to represent all information sets in the game and is therefore only 
 * feasible with games with a small number of different information sets.
 * 
 * @param <S> TurnBasedGameState this algorithm works with
 */
public class CFR<S extends TurnBasedGameState> {
	private Map<S, Map<Action, Double>> regretTables; // Cumulative regret per information set 
	private Map<S, Map<Action, Double>> strategyTables; // The cumulative strategy profile per information set
	private Map<S, Map<Action, Double>> profileT; // The strategy profile (strategies per information set for all players) for this iteration
	private Map<S, Map<Action, Double>> profileTp1; // The strategy profile (strategies per information set for all players) for the next iteration

	private S root;
	private InformationSetDeterminizer<S> det;
	private GameLogic<S> logic;
	private GameFinalEvaluator<S> eval;
	private ActionSensor<S> sensor;

	public CFR(S root, InformationSetDeterminizer<S> det, GameLogic<S> logic, GameFinalEvaluator<S> eval, ActionSensor<S> sensor) {
		this.root = root;
		this.det = det;
		this.logic = logic;
		this.eval = eval;
		this.sensor = sensor;
		this.regretTables = new HashMap<S, Map<Action, Double>>();
		this.strategyTables = new HashMap<S, Map<Action, Double>>();
		this.profileT = new HashMap<S, Map<Action, Double>>(); // Time t (initially, t=0)
		this.profileTp1 = new HashMap<S, Map<Action, Double>>(); // Time t+1
	}
	
	/**
	 * Initializes the regretTable, strategyTable etc. for the information set that the given state belongs to.
	 * The cumulative regret and strategy profile are initialized to 0, while the strategies at this information
	 * set are initialized to a uniform distribution over actions.
	 */
	private void initTable(S state) {
		S infoSetForActivePlayer = det.observeState(state, state.nextMovePlayerID);
		if (this.regretTables.containsKey(infoSetForActivePlayer)) {
			return;
		}
		List<Action> actions = logic.generateActions(infoSetForActivePlayer, state.nextMovePlayerID);
		Map<Action, Double> regretTable = new HashMap<Action, Double>();
		Map<Action, Double> strategyTable = new HashMap<Action, Double>();
		Map<Action, Double> distT = new HashMap<Action, Double>();
		Map<Action, Double> distTp1 = new HashMap<Action, Double>();
		double initialActionProb = 1.0/actions.size();
		for (Action a : actions) {
			regretTable.put(a, 0.0);
			strategyTable.put(a, 0.0);
			distT.put(a, initialActionProb);
			distTp1.put(a, initialActionProb);
		}
		regretTables.put(infoSetForActivePlayer, regretTable);
		strategyTables.put(infoSetForActivePlayer, strategyTable);
		profileT.put(infoSetForActivePlayer, distT);
		profileTp1.put(infoSetForActivePlayer, distTp1);
	}
	
	/**
	 * Builds the complete game TreeEngine and calls initTable for each encountered state.
	 * After this method call regretTable, strategyTable etc. exist for each possible information set in the game.
	 */
	private void initTables() {
		List<S> queue = new LinkedList<S>();
		queue.add(root);
		while (!queue.isEmpty()) {
			S firstState = queue.remove(0);
			initTable(firstState);
			List<Action> actions = logic.generateActions(firstState, firstState.nextMovePlayerID);
			for (Action a : actions) {
				@SuppressWarnings("unchecked")
				S newState = (S) firstState.deepCopy();
				logic.makeMove(newState, sensor.fromPointOfView(a, newState, -1));
				queue.add(newState);
			}
		}
	}
	
	/**
	 * The cfr iteration, line numbers correspond to the pseudo code on page 12 of the above mentioned paper
	 */
	private double cfr(S h, int i, int t, double pi1, double pi2) {
		// Lines 6-7
		if (logic.isTerminal(h)) {
			return eval.getScore(h)[i];
		}
		// TODO Add chance node handling, Lines 8-11, see issue #235

		// Line 12
		S I = det.observeState(h, h.nextMovePlayerID);
		// Line 13
		double vSig = 0.0;
		// Line 14
		Map<Action, Double> vSigIToA = new HashMap<Action, Double>();
		Set<Action> actionsAtI = profileT.get(I).keySet();
		for (Action a : actionsAtI) {
			vSigIToA.put(a, 0.0);
		}

		// Lines 15-22
		for (Action a : actionsAtI) {
			if (h.nextMovePlayerID == 0) {
				@SuppressWarnings("unchecked")
				S ha = (S) h.deepCopy();
				logic.makeMove(ha, sensor.fromPointOfView(a, ha, -1));
				vSigIToA.put(a, cfr(ha, i, t, profileT.get(I).get(a) * pi1, pi2));
			} else if (h.nextMovePlayerID == 1) {
				@SuppressWarnings("unchecked")
				S ha = (S) h.deepCopy();
				logic.makeMove(ha, sensor.fromPointOfView(a, ha, -1));
				vSigIToA.put(a, cfr(ha, i, t, pi1, profileT.get(I).get(a) * pi2));
			}
			vSig = vSig + profileT.get(I).get(a) * vSigIToA.get(a);
		}
		
		// Lines 23-29
		if (h.nextMovePlayerID == i) {
			double sumCumulativeRegrets = 0;
			for (Action a : actionsAtI) {
				regretTables.get(I).put(a, regretTables.get(I).get(a) + (i==1?pi2:pi1) * (vSigIToA.get(a) - vSig));
				strategyTables.get(I).put(a, strategyTables.get(I).get(a) + (i==1?pi1:pi2) * profileT.get(I).get(a));
				sumCumulativeRegrets += Math.max(regretTables.get(I).get(a), 0.0);
			}		
			double uniformProb = 1.0/actionsAtI.size();
			for (Action a : actionsAtI) {
				if (sumCumulativeRegrets > 0) {
					profileTp1.get(I).put(a, Math.max(regretTables.get(I).get(a), 0.0) / sumCumulativeRegrets);						
				} else {
					profileTp1.get(I).put(a, uniformProb);					
				}
			}			
		}
		
		// Line 30
		return vSig;
	}
	
	/**
	 * First, initializes the data structures for each information set in the game.
	 * Then runs the counterfactual regret minimization algorithm for the given number of iterations.
	 * No return value, instead use getCumulativeStrategy and normalize using {@link Helper#getAverageStrategyProfile}
	 */
	public void solve(int numIterations) {
		Log.info("CFR", "Initializing data structures...");
		long startTime = System.nanoTime();
		initTables();
		Log.info("CFR", "Initializing DONE. Time elapsed: " + TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - startTime) + "s. Number of info sets: " + profileT.size());
		for (int t = 0; t < numIterations; t++) {
			startTime = System.nanoTime();
			for (int i = 0; i < 2; i++) {
				cfr(root, i, t, 1, 1);
			}
			profileT = profileTp1; // After each iteration, the profile for t+1 becomes t
			Log.info("CFR", "Iteration " + t + " took " +  TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - startTime) + "s.");
		}
	}
	
	/**
	 * @param I Some information set.
	 * @return Cumulative strategy at this information set.
	 */
	public Map<Action, Double> getCumulativeStrategy(S I) {
		return strategyTables.get(I);
	}

}
