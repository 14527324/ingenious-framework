package za.ac.sun.cs.ingenious.search.mcts;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeComposition;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsNodeExtensionParallelInterface;

import java.util.Hashtable;
import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public final class MctsNodeTreeParallelReentrant<S extends GameState> implements MctsNodeTreeParallelInterface<S, MctsNodeTreeParallelReentrant<S>, MctsNodeTreeParallelReentrant<S>> {

	private final MctsNodeComposition<S, MctsNodeTreeParallelReentrant<S>, MctsNodeTreeParallelReentrant<S>> mctsBasic;

	private double virtualLoss;
	private final Lock lock = new ReentrantLock();
	private final Lock writeLock = lock;
	private final Lock readLock = lock;
	protected int depth;
	private int playerID;

	// lock for enhancement classes
	private final ReadWriteLock enhancementClasses = new ReentrantReadWriteLock();
	private final Lock readLockEnhancementClasses = enhancementClasses.readLock();

	// comment bout children being thread-safe thanks to using CopyOnWriteArrayList concurrency list
	public MctsNodeTreeParallelReentrant(S state, Action prevAction, MctsNodeTreeParallelReentrant<S> parent, List<MctsNodeTreeParallelReentrant<S>> children, GameLogic<S> logic, int playerID) {
		this.playerID = playerID;
		if (children == null) {
			children = new ArrayList<MctsNodeTreeParallelReentrant<S>>();
		}
		mctsBasic = new MctsNodeComposition<>(state, prevAction, parent, children, logic, this::getAnUnexploredChild, this::getChildVisitCount, this::getChildValue, this::getSelfAsChildType, this::incChildVisitCount, true);
		virtualLoss = 0;
	}

	public void logPossibleMoves() {
		for(MctsNodeTreeParallelReentrant<S> node : this.getChildren()){
			MctsNodeTreeParallelReentrant child = (MctsNodeTreeParallelReentrant) node;
			Log.debug("MCTS", child.getPrevAction().toString() + " winrate: " + child.getValue() + "/"
					+ child.getVisitCount() + " value: " + child.getValue()/child.getVisitCount());
		}
	}

	public void readLockEnhancementClassesArrayList() {
		readLockEnhancementClasses.lock();
	}

	public void readUnlockEnhancementClassesArrayList() {
		readLockEnhancementClasses.unlock();
	}

	public Hashtable<String, MctsNodeExtensionParallelInterface> getEnhancementClasses() {
		readLockEnhancementClasses.lock();
		try{
			return mctsBasic.getEnhancementClasses();
		} finally {
			readLockEnhancementClasses.unlock();
		}
	}

	// SearchNode interface:
	public S getState() {
		readLock.lock();
		try {
			return mctsBasic.getState();
		} finally {
			readLock.unlock();
		}
	}

	/**
	 * @return Depth of this node in the search TreeEngine. 0 is the depth of the TreeEngine root.
	 */
	public int getDepth() {
		return depth;
	}
	
	public double getValue() {
		readLock.lock();
		try {
			return mctsBasic.getValue();
		} finally {
			readLock.unlock();
		}
	}
	
	public void addValue(double add) {
		writeLock.lock();
		try {
			mctsBasic.addValue(add);
		} finally {
			writeLock.unlock();
		}
	}
	
	// TODO: change?
	public String toString() {
		String s = "toString MctsNodeTreeParallelReentrant: try failed";
		readLock.lock();
		try {
			String parentString = (this.getParent() == null) ? "null" : this.getParent().getState().toString();
			s = "MctsNodeTreeParallelReentrant:\n\tState = " + getState().toString() + "\n\tvalue = " + getValue() +
			    "\n\tParent (state) = " + parentString + "\n\tChildrenSize = " + getChildren().size() +
			    "\n\tvisitCount = " + getVisitCount() + "\n\tUnexploredChildren = " + getUnexploredChildren() +
			    "\n\tvirtualLoss = " + getVirtualLoss();
		} finally {
			readLock.unlock();
		}
		return s;
	}
	
	// MctsNode interface:
	public MctsNodeTreeParallelReentrant<S> getParent() {
		readLock.lock();
		try {
			return mctsBasic.getParent();
		} finally {
			readLock.unlock();
		}
	}
	
	public void setParent(MctsNodeTreeParallelReentrant<S> parent) {
		writeLock.lock();
		try {
			mctsBasic.setParent(parent);
		} finally {
			writeLock.unlock();
		}
	}
	
	/* returns a thread-safe list, since the children argument 
	   was passes as a CopyOnWriteArrayList when mctsBasic was initialised.
	   The list returned may be iterated without further locks in the calling
	   class after having received the list.
	*/
	public List<MctsNodeTreeParallelReentrant<S>> getChildren() {
		readLock.lock();
		try {
			return mctsBasic.getChildren();
		} finally {
			readLock.unlock();
		}
	}
	
	public void setChildren(List<MctsNodeTreeParallelReentrant<S>> children) {
		writeLock.lock();
		try {
			mctsBasic.setChildren(new CopyOnWriteArrayList(children));
		} finally {
			writeLock.unlock();
		}
	}
	
	// thread-safe because children is a CopyOnWriteArrayList
	public void addChild(MctsNodeTreeParallelReentrant<S> child) {
		/* the write lock is not needed here, since the children list is 
		   thread-safe, so addChild method does not need to block other reading 
		   methods. However, the read lock is used here since the children 
		   list itself may be replaced with a different list by the method 
		   setChildren(), by locking the readlock, it will block setChildren() 
		   from writing whilst also still allowing reading threads to read.
		*/
		readLock.lock();
		try {
			mctsBasic.addChild(child);
		} finally {
			readLock.unlock();
		}
	}


	public void addChildren(List<MctsNodeTreeParallelReentrant<S>> newChildren) {
		// same reasoning as addChild() for reason about read lock
		readLock.lock();
		try {
			mctsBasic.addChildren(newChildren);
		} finally {
			readLock.unlock();
		}
	}
	
	// MctsNode interface:
	public double getVisitCount() {
		readLock.lock();
		try {
			return mctsBasic.getVisitCount();
		} finally {
			readLock.unlock();
		}
	}
	
	public void incVisitCount() {
		writeLock.lock();
		try {
			mctsBasic.incVisitCount();
		} finally {
			writeLock.unlock();
		}
	}
	
	public void decVisitCount() {
		writeLock.lock();
		try {
			mctsBasic.decVisitCount();
		} finally {
			writeLock.unlock();
		}
	}
	
	public void incChildVisitCount(MctsNodeTreeParallelReentrant<S> child) {
		/* child of MctsNodeTreeParrallel is implemented as MctsNodeTreeParrallel,
		   since the incVisitCount() method is thread safe, no locking is required
		   for child.incVisitCount().
		*/
		child.incVisitCount();
	}
	
	public double getChildVisitCount(MctsNodeTreeParallelReentrant<S> child) {
		/* child of MctsNodeTreeParrallel is implemented as MctsNodeTreeParrallel,
		   since the getVisitCount() method is thread safe, no locking is required
		   for child.getVisitCount().
		*/
		return child.getVisitCount();
	}
	
	public double getChildValue(MctsNodeTreeParallelReentrant<S> child) {
		/* child of MctsNodeTreeParrallel is implemented as MctsNodeTreeParrallel,
		   since the getValue() method is thread safe, no locking is required
		   for child.getValue().
		*/
		return child.getValue();
	}
	
	// returns concurrent thread-safe list (CopyOnWriteOnArrayList)
	public List<Action> getUnexploredChildren() {
		readLock.lock();
		try {
			return mctsBasic.getUnexploredChildren();
		} finally {
			readLock.unlock();
		}
	}
	
	public boolean isUnexploredEmpty() {
		readLock.lock();
		try {
			return mctsBasic.isUnexploredEmpty();
		} finally {
			readLock.unlock();
		}
	}
	
	public MctsNodeTreeParallelReentrant<S> getAnUnexploredChild(GameLogic<S> logic) {
		List<Action> unexplored;
		S childState;
		
		readLock.lock();
		try {
			if (this.isUnexploredEmpty()) {
				return null;
			}
		
			// getState() returns defensive copy i.e. not alias, so allowed:
			childState = this.getState();
			unexplored = this.getUnexploredChildren();
		} finally {
			readLock.unlock();
		}
		Action unexploredAction = unexplored.remove((int) (Math.random() * unexplored.size()));
		logic.makeMove(childState, unexploredAction);

		int alternatedPlayer = alternatePlayer(this.getPlayerID());
		return new MctsNodeTreeParallelReentrant<S>(childState, unexploredAction, this, new ArrayList<MctsNodeTreeParallelReentrant<S>>(), logic, alternatedPlayer);
	}

	public MctsNodeTreeParallelReentrant<S> getAnUnexploredChild(GameLogic<S> logic, int index) {
		List<Action> unexplored;
		S childState;

		readLock.lock();
		try {
			if (this.isUnexploredEmpty()) {
				return null;
			}

			// getState() returns defensive copy i.e. not alias, so allowed:
			childState = this.getState();
			unexplored = this.getUnexploredChildren();
		} finally {
			readLock.unlock();
		}
		Action unexploredAction = unexplored.remove(index);
		logic.makeMove(childState, unexploredAction);

		int alternatedPlayer = alternatePlayer(this.getPlayerID());
		return new MctsNodeTreeParallelReentrant<S>(childState, unexploredAction, this, new ArrayList<MctsNodeTreeParallelReentrant<S>>(), logic, alternatedPlayer);
	}

	public int getPlayerID() {
		return mctsBasic.getPlayerID();
	}

	public int alternatePlayer(int player) {
		if (player == 0) {
			return 1;
		} else if (player == 1) {
			return 0;
		} else {
			System.out.println("Shouldn't get here, MctsNodeTreeParallelReentrant invalid player");
			return -1;
		}
	}
	
	public Action getPrevAction() {
		readLock.lock();
		try {
			return mctsBasic.getPrevAction();
		} finally {
			readLock.unlock();
		}
	}
	
	public MctsNodeTreeParallelReentrant<S> getSelfAsChildType() {
		readLock.lock();
		try {
			return this;
		} finally {
			readLock.unlock();
		}
	}
	
	public double getVirtualLoss() {
		readLock.lock();
		try {
			return this.virtualLoss;
		} finally {
			readLock.unlock();
		}
	}
	
	public void applyVirtualLoss(double lossValue) {
		writeLock.lock();
		try {
			this.virtualLoss -= lossValue;
			mctsBasic.addValue(lossValue);
		} finally {
			writeLock.unlock();
		}
	}
	
	public void restoreVirtualLoss(double lossValue) {
		writeLock.lock();
		try {
			this.virtualLoss += lossValue;
			mctsBasic.addValue(-lossValue);
		} finally {
			writeLock.unlock();
		}
	}
	
	public void incVisitVirtualLoss(double lossValue) {
		writeLock.lock();
		try {
			this.virtualLoss -= lossValue;
			mctsBasic.addValue(lossValue);
			mctsBasic.incVisitCount();
		} finally {
			writeLock.unlock();
		}
	}
	
	public void addValueVirtualLoss(double lossValue, double add) {
		writeLock.lock();
		try {
			this.virtualLoss += lossValue;
			mctsBasic.addValue(-lossValue);
			mctsBasic.addValue(add);
		} finally {
			writeLock.unlock();
		}
	}
	
	public void readLock() {
		this.readLock.lock();	
	}
	
	public void readUnlock() {
		this.readLock.unlock();
	}
	
	public void writeLock() {
		this.writeLock.lock();
	}
	
	public void writeUnlock() {
		this.writeLock.unlock();
	}
	
}

// TODO: Comments!
