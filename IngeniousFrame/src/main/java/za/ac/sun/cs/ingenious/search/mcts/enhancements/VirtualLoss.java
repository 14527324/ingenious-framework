package za.ac.sun.cs.ingenious.search.mcts.enhancements;

import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeCompositionInterface;

import za.ac.sun.cs.ingenious.core.util.search.mcts.MctsGameFinalEvaluator;

public class VirtualLoss<S extends GameState, C, N extends MctsNodeCompositionInterface<S, C, ?>> implements EnhancementThreadSafe<S, C, N> {

	MctsGameFinalEvaluator<S> evaluator;

	public VirtualLoss(MctsGameFinalEvaluator<S> evaluator) {
		this.evaluator = evaluator;
	}

	public void postSelection(N node, C child) {
		// apply virtual loss
		node.incChildVisitCount(child);
		//node.addValue(evaluator.getLossValue()); ??
		//node.addChildValue(child); ??? // TODO add to mcts node interface and all over?
		// following if will work for kl stuff, but not Marcs...
		if (child instanceof MctsNodeCompositionInterface<?, ?, ?>) {
			((MctsNodeCompositionInterface<?,?,?>) child).addValue(evaluator.getLossValue());
		}
	}
	
	public void postExpansion(N node, C child) {
		// apply virtual loss to expanded node
		if (child != null) {
			node.incChildVisitCount(child);
			//node.addChildValue(child); ??? // TODO add to mcts node interface and all over?
			// following if will work for kl stuff, but not Marcs...
			if (child instanceof MctsNodeCompositionInterface<?, ?, ?>) {
				((MctsNodeCompositionInterface<?,?,?>) child).addValue(evaluator.getLossValue());
			}
		}
	}
	
	public void preBackpropagation(N node, double[] results) {
		// restore virtual loss
		node.decVisitCount();
		node.addValue(-evaluator.getLossValue());
	}

}

// TODO: comment
