package za.ac.sun.cs.ingenious.search.mcts.expansion;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeCompositionInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MAST.MastTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ExpansionMast<S extends GameState, C, N extends MctsNodeCompositionInterface<S, C, ?>> implements ExpansionThreadSafe<C, N> {

	private GameLogic<S> logic;
	private MastTable visitedMovesTableMast = null;
	private double tau;

	private ExpansionMast(GameLogic<S> logic, MastTable visitedMovesTableMast, double tau) {
		this.logic = logic;
		this.visitedMovesTableMast = visitedMovesTableMast;
		this.tau = tau;
	}

	/**
	 * Create a new child node from the current node and add it to the search tree according to the MAST move ordering.
	 * @param node the current node for which a child must be added
	 * @return
	 */
	public C expand(N node) {

		if (logic.isTerminal(node.getState())) {
			return node.getSelfAsChildType();
		}

		Random randomGen = new Random();
		double bestGibbsValueSoFar = 0;
		Tuple<Action, Double, Integer> randomChoice;
		double denominator;
		List<Action> unexploredActions = node.getUnexploredChildren();

		List<Tuple<Action, Double, Integer>> possibleActions = new ArrayList<>();
		for (int i =0; i < unexploredActions.size(); i++) {
			possibleActions.add(new Tuple<>(unexploredActions.get(i), 0, i));
		}

		denominator = visitedMovesTableMast.getWinToVisitValues();
		for (int i = 0; i < possibleActions.size(); i++) {
			MastTable.StoredAction currentAction = visitedMovesTableMast.getMoveCombinationScores().get(possibleActions.get(i).getAction());

			double numerator;
			double currentGibbsValue;
			if (possibleActions.get(i).getAction().getPlayerID() != -1) {
				if (currentAction != null) {
					// If the action has already been explored, calculate the gibbs value
					numerator = Math.exp((currentAction.getWins()/currentAction.getVisitCount())/tau);
					currentGibbsValue = (numerator/denominator);
				} else {
					// If the action has not already been explored, bias selection towards this unexplored action
					numerator = Math.exp((1)/tau);
					currentGibbsValue = (numerator/denominator);
				}
				if (currentGibbsValue >= bestGibbsValueSoFar) {
					bestGibbsValueSoFar = currentGibbsValue;
					possibleActions.get(i).updateGibbs(currentGibbsValue);
				}
			}
		}

		List<Tuple<Action, Double, Integer>> bestGibbs = new ArrayList<>();
		for (int i = 0; i < possibleActions.size(); i++) {
			if (possibleActions.get(i).getGibbs() == bestGibbsValueSoFar) {
				bestGibbs.add(possibleActions.get(i));
			}
		}

		int randomNumber = randomGen.nextInt(bestGibbs.size());
		randomChoice = bestGibbs.get(randomNumber);
		C expandedChild;
		expandedChild = node.getAnUnexploredChild(logic, randomChoice.getIndex());

		if (expandedChild == null) {
			return null;
		}
		node.addChild(expandedChild);
		return expandedChild;
	}
	
	public static <SS extends GameState, CC, NN extends MctsNodeCompositionInterface<SS,CC,?>> ExpansionThreadSafe<CC, NN> newExpansionMast(GameLogic<SS> logic, MastTable visitedMovesTableMast, double tau) {
		return new ExpansionMast<>(logic, visitedMovesTableMast, tau);
	}

	/**
	 * Helper object - an implementation of a generic tuple object, which stores
	 * two object elements in as a tuple.
	 */
	private class Tuple<Action, Double, Integer> {
		Action action;
		double gibbs;
		int index;

		Tuple(Action a, double g, int index) {
			this.action = a;
			this.gibbs = g;
			this.index = index;
		}

		void updateGibbs(double g) {
			gibbs = g;
		}

		double getGibbs() {
			return gibbs;
		}

		Action getAction() {
			return action;
		}

		int getIndex() {
			return index;
		}
	}
}
