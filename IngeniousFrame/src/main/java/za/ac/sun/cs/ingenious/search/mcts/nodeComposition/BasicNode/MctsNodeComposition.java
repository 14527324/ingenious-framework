package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode;

import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.util.search.TreeNode;
import za.ac.sun.cs.ingenious.core.util.search.TreeNodeComposition;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsNodeExtensionParallelInterface;

import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.Consumer;
import java.util.concurrent.CopyOnWriteArrayList;

public class MctsNodeComposition<S extends GameState, C, P> implements MctsNodeCompositionInterface<S, C, P> {

	private TreeNode<S, C, P> treeNode;
	private double visitCount;
	private List<Action> unexploredChildren = null;
	private Action prevAction;
	private Function<GameLogic<S>, C> unexploredChildMethod;
	private Function<C, Double> childVisitCountMethod;
	private Function<C, Double> childValueMethod;
	private Supplier<C> getAsChildTypeMethod;
	private Consumer<C> incChildVisitCountMethod;
	private int playerID;

	private Hashtable<String, MctsNodeExtensionParallelInterface> enhancementClasses = null;

	/** constructor used by classes utilising the new structure */
	public MctsNodeComposition(S state, Action prevAction, P parent, List<C> children, GameLogic<S> logic, Function<GameLogic<S>, C> unexploredChildMethod, Function<C, Double> childVisitCountMethod, Function<C, Double> childValueMethod, Supplier<C> getAsChildTypeMethod, Consumer<C> incChildVisitCountMethod, boolean makeListsParallel, Hashtable<String, MctsNodeExtensionParallelInterface> enhancementClasses, int playerID) {

		this.playerID = playerID;

		treeNode = (makeListsParallel) ? new TreeNodeComposition<S, C, P>(state, parent, new CopyOnWriteArrayList(children)) :
				new TreeNodeComposition<>(state, parent, children);
		this.prevAction = prevAction;
		this.visitCount = 0;

		MctsNodeTreeParallel parentNode = (MctsNodeTreeParallel)parent;
		if (parentNode != null && parentNode.getPrevAction() != null) {
			unexploredChildren = logic.generateActions(state, playerID);
		} else {
			unexploredChildren = logic.generateActions(state, playerID);
		}

		if (makeListsParallel) {
			unexploredChildren = Collections.synchronizedList(unexploredChildren);
		}
		this.unexploredChildMethod = unexploredChildMethod;
		this.childVisitCountMethod = childVisitCountMethod;
		this.childValueMethod = childValueMethod;
		this.getAsChildTypeMethod = getAsChildTypeMethod;
		this.incChildVisitCountMethod = incChildVisitCountMethod;

		this.enhancementClasses = enhancementClasses;
	}

	/** constructor left in for classes still utilising the old structure */
	public MctsNodeComposition(S state, Action prevAction, P parent, List<C> children, GameLogic<S> logic, Function<GameLogic<S>, C> unexploredChildMethod, Function<C, Double> childVisitCountMethod, Function<C, Double> childValueMethod, Supplier<C> getAsChildTypeMethod, Consumer<C> incChildVisitCountMethod, boolean makeListsParallel) {
		treeNode = (makeListsParallel) ? new TreeNodeComposition<S, C, P>(state, parent, new CopyOnWriteArrayList(children)) :
										 new TreeNodeComposition<>(state, parent, children);
		this.prevAction = prevAction;
		this.visitCount = 0;
		MctsNodeTreeParallel parentNode = (MctsNodeTreeParallel)parent;

		if (parentNode != null && parentNode.getPrevAction() != null) {
			unexploredChildren = logic.generateActions(state, playerID);
		} else {
			unexploredChildren = logic.generateActions(state, playerID);
		}
		if (makeListsParallel) {
			unexploredChildren = Collections.synchronizedList(unexploredChildren);
		}
		this.unexploredChildMethod = unexploredChildMethod;
		this.childVisitCountMethod = childVisitCountMethod;
		this.childValueMethod = childValueMethod;
		this.getAsChildTypeMethod = getAsChildTypeMethod;
		this.incChildVisitCountMethod = incChildVisitCountMethod;
	}
	
	public MctsNodeComposition(S state, Action prevAction, P parent, List<C> children, GameLogic<S> logic, Function<GameLogic<S>, C> unexploredChildMethod, Function<C, Double> childVisitCountMethod, Function<C, Double> childValueMethod, Supplier<C> getAsChildTypeMethod, Consumer<C> incChildVisitCountMethod) {
		this(state, prevAction, parent, children, logic, unexploredChildMethod, childVisitCountMethod, childValueMethod, getAsChildTypeMethod, incChildVisitCountMethod, false);
	}

	// TODO: change?
	public String toString() {
		getState().printPretty();
		return ("TreeNodeNode:\n\tState = " + getState().toString() + "\n\tvalue = " + getValue() +
		        "\n\tParent (state) = " + this.getParent() + "\n\tChildren = " + getChildren() +
		        "\n\tvisitCount = " + getVisitCount() + "\n\tUnexploredChildren = " + getUnexploredChildren());
	}
	public Hashtable<String, MctsNodeExtensionParallelInterface> getEnhancementClasses() {
		return enhancementClasses;
	}

	/**
	 * @return the state relating to the current node.
	 */
	public S getState() {
		return treeNode.getState();
	}

	/**
	 * @return the number of winning playouts played through the current node.
	 */
	public double getValue() {
		return treeNode.getValue();
	}

	/**
	 * Add the inputted value to the win count for the current node.
	 * @param add
	 */
	public void addValue(double add) {
		treeNode.addValue(add);
	}

	/**
	 * @return the player relating to this current node, i.e. the player that must play next
	 * from the current game state.
	 */
	public int getPlayerID() {
		return playerID;
	}

	/**
	 * @return the current node's parent.
	 */
	public P getParent() {
		return treeNode.getParent();
	}

	/**
	 * Set the current node's parent to the inputted node.
	 * @param parent
	 */
	public void setParent(P parent) {
		treeNode.setParent(parent);
	}

	/**
	 * @return the current node's set of children
	 */
	public List<C> getChildren() {
		return treeNode.getChildren();
	}

	/**
	 * Set the current node's list of children to the inputted set of children.
	 * @param children
	 */
	public void setChildren(List<C> children) {
		treeNode.setChildren(children);
	}

	/**
	 * Add the child to the current node's set of children
	 * @param child
	 */
	public void addChild(C child) {
		treeNode.addChild(child);
	}

	/**
	 * Add the list of children to the current node's set of children
	 * @param newChildren
	 */
	public void addChildren(List<C> newChildren) {
		treeNode.addChildren(newChildren);
	}

	/**
	 * return the visit count for this node
	 * @return
	 */
	public double getVisitCount() {
		return this.visitCount;
	}

	/**
	 * increment the visit count of the current node by 1
	 */
	public void incVisitCount() {
		visitCount++;
	}

	/**
	 * decrement the visit count of the current node by 1
	 */
	public void decVisitCount() {
		visitCount--;
	}

	/**
	 * @param child
	 */
	public void incChildVisitCount(C child) {
		this.incChildVisitCountMethod.accept(child);
	}

	/**
	 * @param child
	 * @return the number of times the child relating to the current node has been visited.
	 */
	public double getChildVisitCount(C child) {
		return this.childVisitCountMethod.apply(child);
	}

	/**
	 * @param child
	 * @return the number of times the child relating to the current node has had a winning playout.
	 */
	public double getChildValue(C child) {
		return this.childValueMethod.apply(child);
	}

	/**
	 * @return the array of unexplored actions relating to children that can be expanded from this current node
	 */
	public List<Action> getUnexploredChildren() {
		return this.unexploredChildren;
	}

	/**
	 * @return true if there are no more children to expland, false otherwise
	 */
	public boolean isUnexploredEmpty() {
		return unexploredChildren.isEmpty();
	}

	/**
	 * @return the action most recently made on the current node's board state
	 */
	public Action getPrevAction() {
		return this.prevAction;
	}

	/**
	 * @return the current node case as a child type object
	 */
	public C getSelfAsChildType() {
		return this.getAsChildTypeMethod.get();
	}

	/**
	 * Return a child object by choosing a random action in the set of unexplored actions and creating a child node
	 * corresponding to that action.
	 * @param logic
	 * @return
	 */
	public C getAnUnexploredChild(GameLogic<S> logic)  {
		if (isUnexploredEmpty()) {
			return null;
		}
		S childState = getState();

		Action unexploredAction = unexploredChildren.remove((int) (Math.random() * unexploredChildren.size()));
		logic.makeMove(childState, unexploredAction);
		
		return this.unexploredChildMethod.apply(logic);
	}

	/**
	 * Return a child object by choosing an based on the index parameter for an action in the set of unexplored actions and creating a child node
	 * corresponding to that action.
	 * @param logic
	 * @param index the index into the array of unexplored actions for which the child node must be created.
	 * @return
	 */
	public C getAnUnexploredChild(GameLogic<S> logic, int index) {
		if (isUnexploredEmpty()) {
			return null;
		}
		S childState = getState();
		Action unexploredAction = unexploredChildren.remove(index);
		logic.makeMove(childState, unexploredAction);

		return this.unexploredChildMethod.apply(logic);
	}
}
