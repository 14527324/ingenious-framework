package za.ac.sun.cs.ingenious.search.mcts.simulation;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;

import java.util.ArrayList;

/**
 * The simulation strategy determines how a MCTS self-play simulation playout is
 * run.
 *
 * @author Karen Laubscher
 * 
 * @param <S> The game state type
 */
public interface SimulationThreadSafe<S extends GameState> {
	
	/**
	 * The method that performs a simulation playout.
	 *
	 * @param state  The game state from which to start the simulation playout.
	 *
	 * @return  The result scores
	 */
	SimulationTuple simulate(S state);

	ArrayList<Action> getMoves();
	
}
