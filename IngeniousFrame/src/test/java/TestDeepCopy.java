import static org.junit.Assert.assertTrue;

import org.junit.Test;

import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;

public class TestDeepCopy {

	@Test
	public void testTurnBasedSquareBoard() {
		TurnBasedSquareBoard b = new TurnBasedSquareBoard(3, 0, 2);
		b.board[1]=9;		
		TurnBasedSquareBoard b2 = (TurnBasedSquareBoard) b.deepCopy();
		assertTrue(b2.board[1]==9);		
		b.board[2] = 8;
		b2.board[5] = 7;
		assertTrue(b2.board[2]!=8);
		assertTrue(b.board[5]!=7);
	}

}
